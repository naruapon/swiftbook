// listing 3.9
var year = 3
var price = 62.54
print(type(of: year))   // Int
print(type(of: price)) // Double

//listing 3.11

print("UInt8 max \(UInt8.max)")   // "UInt8 max 255"
print("UInt8 min \(UInt8.min)")   //  "UInt8 min 0"

print("UInt16 max \(UInt16.max)")  // "UInt16 max 65535"
print("UInt16 min \(UInt16.min)")  // "UInt16 min 0"

print("UIn32 max \(UInt32.max)")  // "UInt32 max 4294967295"
print("UInt32 min \(UInt32.min)")  // "UInt32 min 0"

print("UInt64 max \(UInt64.max)")  // "UInt64 max 18446744073709551615"
print("UInt64 min \(UInt64.min)")  // "UInt64 min 0"

print("UInt max \(UInt.max)")  // "UInt max 18446744073709551615"
print("UInt min \(UInt.min)")  // "UInt min 0"

print("Int8 max \(Int8.max)")  // "Int8 max 127"
print("Int8 min \(Int8.min)")  // "Int8 min -128"
print("Int16 max \(Int16.max)")  // "Int16 max 32767"
print("Int16 min \(Int16.min)")  // "Int16 min -32768"

print("Int32 max \(Int32.max)")  // "Int32 max 2147483647"
print("Int32 min \(Int32.min)")  // "Int32 min -2147483648"

print("Int64 max \(Int64.max)")  // "Int64 max 9223372036854775807"
print("Int64 min \(Int64.min)")  // "Int64 min -9223372036854775808"

print("Int max \(Int.max)")  // "Int max 9223372036854775807"
print("Int min \(Int.min)") // "Int min -9223372036854775808"

//listing 3.12
var a = 95
var b = 0b1011111   // 95
var c = 0o137   // 95
var d = 0x5f    //95
print(a)
print(b)
print(c)
print(d)

//listing 3.12-2
var value = 1_000_000   // มีค่าเท่ากับ 1000000
print(value)

//listing 3.15
var title = "Swift"
title = title + " Language"
print(title) // Swift Language

//listing 3.16
var header = "Swift"
header += " Language"
print(header)

//listing 3.17
var version = 5
var label = "Swift Language version \(version)"
print(label)

//listing 3.18
var minute = 5
let second = 60
var display = "\(minute) minutes is \(minute*second) seconds"
print(display)

//listing 3.19
var example = "Swift\n\u{050}rogramming"
print(example)

//listing 3.28
var number = Int(14)
var distance = Double(43.25)
print(number)
print(distance)

//listing 3.29
var number1 = 5
var number2 = 10.25
var sum  = Double(number1)/number2
print(sum)

//listing 3.30
var distance = "32"
    if let number = Int(distance){
        let sum = number + 40
        print("Summary is \(sum)")    // "Summary is 72"
    }


//listing 3.33
var product = ("Apple", "iPhoneX", 2017)
var data = "In \(product.2), \(product.0) sold \(product.1)"
print(data)


//listing 3.34
var product = ("Apple", "iPhoneX", 2017)
    product.1 = "iPhone SE 2"
    product.2 = 2018
var data = "\(product.1) could launch in \(product.2)"
print(data)

//listing 3.35
var product = (company:"Apple", productname:"iPhoneX", year:2017)
var data = "In \(product.year), \(product.company) sold \(product.productname)"
print(data)

//listing 3.36
var product = ("Apple", "iPhoneX", 2017)
    var (company, productname, year) = product
var data = "In \(year), \(company) sold \(productname)"
print(data)


//listing 3.37
var product = ("Apple", "iPhoneX", 2017)
    var (company, productname, _) = product
var data = "\(company) sold \(productname)"
print(data)


//listing 3.38
var number = 3 + 2
print(number)


//listing 3.39
var number1 = 3 + 2
var number2 = 3 + 2 * 5
var number3 = 10.0 + 2.0
print(number1)
print(number2)
print(number3)

//listing 3.40
var number1 = 7.0 + 2.0
var number2 = 7 / 2.0
var number3 = 7 / 2
print(number1)
print(number2)
print(number3)

//listing 3.41
var number1 =  10 % 3
var number2 =  8 % 5
print(number1)
print(number2)


//listing 3.42
var number1 =  2
var sum =  number1 + 5
print(sum)

//listing 3.43
var number1 =  2
number1 = number1 + 5
print(number1)

//listing 3.44
var number1 =  2
number1 +=  5
print(number1)


//listing 4.1
var speed = 110
var warning = "Driving under the speed limit"
if speed > 100 {
    warning = "Driving over the speed limit"
}

//listing 4.2
var speed = 100
var warning = "Driving under the speed limit"
if speed >= 100 {
    warning = "Driving over the speed limit"
}
print(warning)

//listing 4.3
var overspeed = true
var warning = "Driving under the speed limit"
if overspeed {
    warning = "Driving over the speed limit"
}
print(warning)

//listing 4.4
var overspeed = true
var warning = "Driving under the speed limit"
if !overspeed {
    warning = "Driving over the speed limit"
}
print(warning)


//listing 4.5
var speed = 200
var supercar = true
var warning = "Driving slow"
if (speed >= 200) && supercar  {
    warning = "Driving fast"
}
print(warning)

//listing 4.6
var sum = 0
var number : Int? = 10
if number != nil {
    let value = number!
    sum = sum + value
}
print(sum)

//listing 4.7
var sum = 0
var number : Int? = 10
if let value = number {
    sum = sum + value
}
print(sum)

//listing 4.8
var sum = 0
var number : Int? = 10
if let value = number {
    sum = sum + value
}
print(sum)


//listing 4.9
var number = 8
if number % 2 == 0 {
    print("Even number")
} else {
    print("Odd number")
}

//listing 4.10
var weight = 52
var data =  "You are "
if weight <= 45 {
    data += "Slim"
} else if weight <= 55 {
    data += " Normal"
} else if weight <= 70 {
    data += " Fat"
} else {
    data += " overweight"
}
print(data)


//listing 4.11
var year = 2
var data = ""
switch year {
    case 1 :
        data = "Freshy"
    case 2 :
        data = "Sophomore"
    case 3:
        data = "Junior"
    case 4 :
        data = "Senior"
    default:
        data = "Sil"
}
print(data)


//listing 4.12
var color = "blue"
var data = ""
switch color {
    case "red", "yellow", "blue", "green" :
        data = "Colorful"
    case "black", "white", "gray" :
        data = "Pale"
    default:
        data = "Out of Scope"
}
print(data)


//listing 4.13
var time = 13
var data = ""
switch time {
case 5...11 :
        data = "Good Morning"
case 12...17 :
        data = "Good Afternoon"

case 18..<21 :
        data = "Good Evening"
    default:
        data = "Night"
}
print(data)

/listing 4.14
var data = ""
var coordinates = (1,5)
switch coordinates {
    case (1,1) :
        data = "Nearly"
    case (1,5) :
        data = "Exactly"
    case (2,9):
        data = "Faraway"
    default:
        data = "Miss"
}
print(data)

//listing 4.15
var data = ""
var coordinates = (1,5)
switch coordinates {
    case (_,1) :
        data = "Nearly"
    case (_,5) :
        data = "Exactly"
    case (_,9):
        data = "Faraway"
    default:
        data = "Miss"
}
print(data)


//listing 4.16
var data = ""
var coordinates = (1,25)
switch coordinates {
case (_,1...5) :
        data = "Nearly"
case (_,6...20) :
        data = "Exactly"
case (_,21...30):
        data = "Faraway"
    default:
        data = "Miss"
}
print(data)


//listing 4.17
var data = ""
var coordinates = (5,25)
switch coordinates {
    case (let x, 5) :
        data = "coordinate order is \(x)"
    case (let x, 25) :
        data = "First order is \(x)"
    case (let x, 30):
        data = "order is \(x)"
    default:
        data = "Miss"
}
print(data)


//listing 4.18
var data = ""
var coordinates = (5,25)
switch coordinates {
case (let x, let y) where x > y :
        data = "first order is greater than second order"
case (let x, let y) where x == y :
        data = "first order equals second order"
case (let x, let y) where x < y :
        data = "first order is less than second order"
    default:
        data = "Out of Scope"
}
print(data)


//listing 4.19
var counter = 0
while counter < 5 {
    counter += 1
}
print(counter)


//listing 4.20
var counter = 10
repeat {
    counter += 1
} while counter < 5
print(counter)


//listing 4.22
var beverages = ["Pepsi", "Coke", "Milk"]
var data = "Beverages : "
for beverage in beverages {
    data += " "+beverage
}
print(data)

//listing 4.23
var beverages = ["Pepsi", "Coke", "Milk"]
var counter = 0
for _ in beverages {
    counter += 1
}
var data = "We have \(counter) beverages" // We have 3 beverages
print(data)

//listing 4.24
var beverages = ["b1":"Pepsi", "b2":"Coke"]
var data = "Beverages : "
for (bindex, beverage) in beverages {
    data += " \(bindex)=\(beverage)"
}
print(data)

//listing 4.25
var total = 0
for value in 0...5 {
    total += value
}
print(total)


//listing 4.26
var total = 0
for _ in 0...5 {
    total += 1   //  เริ่มจาก 0 บวก 1 เข้าไป  6 รอบที่ตัวแปร total
}
print(total)


//listing 5.1
var arraylist : [Int] = [3, 5, 7, 9]
print(arraylist)


//listing 5.2
var arraylist = [3, 5, 7, 9]
print(arraylist)

//listing 5.3
var arraylist = [3, 5, 7, 9]
print(arraylist[2])

//listing 5.4
var arraylist = [3, 5, 7, 9]
arraylist[3] = 11
print(arraylist[3])

//listing 5.5
var arraylist = [3, 5, 7, 9]
arraylist += [11, 17]
print(arraylist)

//listing 5.6
var arraylist1 = [3, 5, 7, 9]
var arraylist2 = [2, 4, 6, 8]
var arraylist3 = arraylist1 + arraylist2
print(arraylist3)


//listing 5.7
var multiarray : [[Int]] = [[1, 3, 5], [2, 4], [5, 10]]
print(multiarray)

//listing 5.8
var multiarray : [[Int]] = [[1, 3, 5], [2, 4], [5, 10]]
print(multiarray[1][0])

//listing 5.9
var arraylist = [3, 5, 7, 9]
arraylist = []
print(arraylist)

//listing 5.10
var scores: Set = [78, 44, 57, 81, 57]
print(scores)


//listing 5.12
var Set1 : Set = ["Swift", "Java", "Python", "HTML"]
var Set2 : Set = ["Java", "TypeScript", "Kotlin"]
print(Set1.intersection(Set2))
print(Set1.symmetricDifference(Set2))
print(Set1.union(Set2))
print(Set1.subtracting(Set2))


//listing 5.13
var devicelist : [String:String] = ["TV":"Samsung", "Mobile":"iPhone"]
print(devicelist)


//listing 5.14
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
print(devicelist)


//listing 5.15
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
devicelist["Mobile"] = "Samsung"
print(devicelist)

//listing 5.16
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
devicelist["Notebook"] = "HP"
print(devicelist)

//listing 5.17
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
print(devicelist["Notebook"])

//listing 5.18
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
print("I have \(devicelist["Car"]!) Car and \(devicelist["Mobile"]!)")

//listing 5.19
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
devicelist["Car"] = nil
print(devicelist)


//listing 5.20
var devicelist = ["Car":"Ford", "Mobile":"iPhone"]
devicelist = [:]
print(devicelist)


//listing 6.1
var number = 6
func multiply() {
    number = number * 4
}
multiply()
print(number)
multiply()
print(number)


//listing 6.2
var number = 6
func multiply() {
    number = number * 4
}
for _ in 0..<3 {
    multiply()
}
print(number)

//listing 6.3
func multiply(number:Int) {
    let result = number * 2
    let message = "Result : \(result)"
    print(message)
}
multiply(number:6)  // "Result : 12"

//listing 6.4
func multiply(number:Int) {
    let result = number * 2
    let message = "Result : \(result)"
    print(message)
}
multiply(number:6) // "Result : 12"
multiply(number:10) // "Result : 20"


//listing 6.5
func multiply(number:Int) -> Int {
    let result = number * 4
    return result
}
let data = multiply(number:6)
let show = "Result : \(data)"  // "Result : 24"
print(show)

//listing 6.6
func sumall(list:[Int]) -> Int {
    var total = 0
    for number in list {
        total = total + number
    }
    return total
}
var numberlist = [2, 4, 6, 8, 10]
var data = "Result : \(sumall(list: numberlist))" // "Result : 30"
print(data)


//listing 6.7
func sumall(list:[Int]) -> (Int, Int) {
    var count = 0
var total = 0
    for number in list {
        total = total + number
        count += 1
    }
    return (count, total)
}
var numberlist = [2, 4, 6, 8, 10]
var (amount, result) = sumall(list: numberlist)
var data = "\(amount) elements and result : \(result))" // "5 elements and result : 30"
print(data)


//listing 6.8
func multiply(value number:Int) -> Int {
    let result = number * 4
    return result
}
let data = multiply(value:6)
let show = "Result : \(data)"  // "Result : 24"
print(show)

//listing 6.9
func multiply(number1:Int,_ number2:Int) -> Int {
    let result = number1 * number2
    return result
}
let data = multiply(number1:6, 5)
let show = "Result : \(data)"  // "Result : 30"
print(show)


//listing 6.10
func multiply(number1:Int = 9) -> Int {
    let result = number1 * 2
    return result
}
let data = multiply()
let show = "Result : \(data)"  // "Result : 18"
print(show)


//listing 6.11
let message = {
    print("Hello Closure");
}
// การเรียกใช้งาน
message()

//listing 6.12
let message:(String) -> () = { param1 in
    print("Hello , \(param1)!")
}
message("World")


//listing 6.13
func mainfunction() -> Int {
    var counter = 0
    func subfunction() -> Int {
        counter += 1
        return counter
    }
    return subfunction()
}
let sum = mainfunction()
print("Result is \(sum)")


//listing 6-14
func mainfunction() -> Int {
    var counter = 0
    func subfunction() -> Int {
        counter += 1
        return counter
    }
    return subfunction()
}
let sum = mainfunction()
print("Result is \(sum)")


//listing 6.15
func mainfunction()-> () -> Int{
    var counter = 0
    return { ()->  Int in
        counter += 1
        return counter
    }
}
let sum = mainfunction()
print("Result is \(sum() )")  // "Result is 1\n"

//listing 6.16
func mainfunction()-> (Int) -> Int{
    var counter = 0
    return { (number:Int)->  Int in
        counter += number
        return counter
    }
}
let sum = mainfunction()
print("Result is \(sum(5) )")  //  "Result is 5\n" 

//listing 6.17
func mainfunction()-> (Int) -> Int{
    var counter = 0
    let result = { (number:Int)->  Int in
        counter += number
        return counter
    }
    return result
}
let sum = mainfunction()
print("Result is \(sum(5) )")  //  "Result is 5\n"

//listing 6.18
let beverages = ["Coke","Pepsi","Beer","Pepsi","Milk"]
let result = { () -> Int in
    var counter = 0
    for beverage in beverages {
        if beverage == "Pepsi" {
            counter += 1
        }
    }
    return counter
}()
print("There are \(result) bottles of Pepsi") // "There are 2 bottles of Pepsi\n"

//listing 6.19
let beverages = ["Coke","Pepsi","Beer","Pepsi","Milk"]
let result : Int = {
    var counter = 0
    for beverage in beverages {
        if beverage == "Pepsi" {
            counter += 1
        }
    }
    return counter
}()
print("There are \(result) bottles of Pepsi") // "There are 2 bottles of Pepsi\n"

//listing 6.20
func showMessage(data: Int){
    print("Data is \(data)")
}
func showMessage(data: String){
    print("Data is \(data)")
}
showMessage(data: 4)    // "Data is 4"
showMessage(data: "Swift") //"Data is Swift"

//listing 6.21
func showMessage<T>(data:T){
    print("Data is \(data)")
}
showMessage(data: 4)    // "Data is 4"
showMessage(data: "Swift") // "Data is Swift"
showMessage(data: 4.2) // "Data is 4.2"

//listing 6.22
var positivenumber = abs(-9)
print("The absolute number is : \(positivenumber)")

//listing 6.23
let number1 = 10
let number2 = 15
let largest = max(number1, number2)
let smallest = min(number1, number2)
print(largest)
print(smallest)

//listing 6.24
let list = stride(from: 10, to: 50, by: 10)
for  x in list {
    print(x)     // 10 20 30 40
}


//listing 7.2
struct Product {
    var amount : Int = 0
    var name : String = "Empty"
    var price : Double = 0.0
}
var product1 : Product = Product()
print(product1)

//listing 7.3
struct Product {
    var amount : Int = 0
    var name : String = "Empty"
    var price : Double = 0.0
}
var product1 : Product = Product()
product1.amount = 5
product1.name = "iPhone7"
product1.price = 21900.00
var totalValue = Double(product1.amount) * product1.price
print("Total value of \(product1.name) is \(totalValue) baht")

//listing 7.4
struct Price {
    var TH = 0.0
    var USD = 0.0
}
struct Product {
    var amount : Int = 0
    var name : String = "Empty"
    var price = Price()
}
var product1 = Product()
product1.amount = 5
product1.name = "iPhone7"
product1.price.TH = 21900.00
product1.price.USD = 644
print(product1)

//listing 7.5
struct Product {
    var amount : Int = 0
    var name : String = "Empty"
    var price : Double = 0.0
    func total() -> Double {
        let totalValue = Double(amount) * price
        return totalValue
    }
}
var product1 : Product = Product()
product1.amount = 5
product1.name = "iPhone7"
product1.price = 21900.00
print("Total Value : \(product1.total())") // "Total Value : 109500.0"

//listing 7.6
struct Product {
        var amount : Int = 0
        var name : String = "Empty"
        var price : Double = 0.0
        mutating func total() {
            let totalValue = Double(amount) * price
            name += " \(totalValue) Baht"
        }
    }
var product1 : Product = Product()
product1.amount = 5
product1.name = "iPhone7"
product1.price = 21900.00
product1.total()
print("Total Value : \(product1.name)") // "Total Value : iPhone7 109500.0 Baht"

//listing 7.7
struct Product {
    var amount : Int = 0
    var name : String = "Empty"
    var price : Double = 0.0
}
var product1 = Product(amount : 5, name : "iPhone7", price : 21900.00)
var total = Double(product1.amount) * product1.price
print("Total Value : \(total)") 

//listing 7.8
struct Product {
        var amount : Int
        var name : String
        var price : Double
}
var product1 = Product(amount : 5, name : "iPhone7", price : 21900.00)
var total = Double(product1.amount) * product1.price
print("Total Value : \(product1.name) \(total)")

//listing 7.9
struct Price {
    var USD : Double
    var TH : Double
    init() {
        USD = 5.99
        TH =  USD * 34.55
    }
}
var productPrice = Price()
print(productPrice.USD)  // 5.99
print(productPrice.TH)  // 206.9545

//listing 7.10
struct Price {
    var USD : Double
    var TH : Double
    init(usa:Double) {
        USD = usa
        TH =  USD * 34.55
    }
}
var productPrice = Price(usa: 5)
print(productPrice.USD)  // 5.0
print(productPrice.TH)  // 172.75

//listing 7.11
struct Price {
    var USD : Double
    var TH : Double
    init(usa:Double) {
        USD = usa
        TH =  USD * 32.84
    }
    init(thai:Double) {
        TH = thai
        USD = thai * 0.030451
    }
}
var productPrice = Price(thai: 5000)
print(productPrice.USD)  // 152.255
print(productPrice.TH)  // 5000.0


//listing 7.12
struct Price {
    var TH : Double
    lazy var rate: Double = {
        return 34.55
    }()
    init(money:Double) {
        TH = money
    }
}
var price = Price(money: 5.99)
print(price)

//listing 7.13
struct Price {
    var TH : Double
    private var rate = 34.55
    init(money:Double) {
        TH = money
    }
    func getRate() -> Double {
        let total = TH * rate
        return total
    }
}
var price = Price(money: 5.99)
print(price.getRate())   // 206.9545


//listing 7.14
protocol ShowInfo {
    var name : String { get set }
    func showDetail()
}
struct Employee : ShowInfo {
    var name : String
    var age : Int
    func showDetail() {
        print("Data : \(name) \(age)")
    }
}
let emp1 = Employee(name: "Weerachart", age : 35)
emp1.showDetail()    // "Data : Weerachart 35"

//listing 7.15
protocol ShowInfo {
    var name : String { get set }
    func showDetail()
}
struct Employee : ShowInfo {
    var name : String
    var age : Int
    func showDetail() {
        print("Data : \(name) \(age)")
    }
}
struct Department : ShowInfo {
    var name : String
    var members : Int
    func showDetail() {
        print("Data : \(name) \(members)")
    }
}
let emp1 = Employee(name: "Weerachart", age : 35)
let dept1 = Department(name: "Marketing", members : 10)
dept1.showDetail()  // "Data : Marketing 10"

//liting 7.16
protocol ShowInfo {
    var name : String { get set }
    func showDetail()
}
struct Employee : ShowInfo {
    var name : String
    var age : Int
    func showDetail() {
        print("Data : \(name) \(age)")
    }
}
struct Department : ShowInfo {
    var name : String
    var members : Int
    func showDetail() {
        print("Data : \(name) \(members)")
    }
}
let emp1 = Employee(name: "Weerachart", age : 35)
let dept1 = Department(name: "Marketing", members : 10)
var list:[ShowInfo] = [emp1, dept1]
for data in list {
    data.showDetail()
}

//listing 7.17
protocol ShowInfo {
    var name : String { get set }
}
extension ShowInfo {
    func showDetail() {
        print("Data : \(name)")
    }
}
struct Employee : ShowInfo {
    var name : String
    var age : Int
}
struct Department : ShowInfo {
    var name : String
    var members : Int
}
let emp1 = Employee(name: "Weerachart", age : 35)
let dept1 = Department(name: "Marketing", members : 10)
emp1.showDetail()    // "Data : Weerachart"
dept1.showDetail()  // "Data : Marketing"

//listing 7.18
extension Int {
    func printDetail() {
        print("Data : \(self)")
    }
}
let number =  19
number.printDetail()   // "Data : 19"

//listing 8.2
class Employee {
    var name = "Empty"
    var age = 0
}
let emp1:Employee = Employee()
print(emp1.name)
print(emp1.age)

//listing 8.3
class Employee {
    var name = "Empty"
    var age = 0
}
let emp1: Employee = Employee()
emp1.name = "Weerachart"
emp1.age = 35
print(emp1.name)
print(emp1.age)

//listing 8.4
class Employee {
    var name = "Empty"
    var age = 0
    func changeValues(newname:String, newage:Int) {
        name = newname
        age = newage
    }
}
let emp1: Employee = Employee()
emp1.changeValues(newname:"Weerachat", newage:35)
print(emp1.name)
print(emp1.age)

//listing 8.5
struct Employee {
    var name = "Empty"
}
var emp1 = Employee()
var emp2 = emp1
emp2.name = "Somchai"
print("Employee1 Name: \(emp1.name)") // "Employee1 Name: Empty"

//listing 8.6
class Employee {
    var name = "Empty"
}
let emp1 = Employee()
let emp2 = emp1
emp2.name = "Somchai"
print("Employee1 Name: \(emp1.name)") // "Employee1 Name: Somchai"

//listing 8.7
class Employee {
    var name = "Empty"
}
let emp1 = Employee()
let emp2 = Employee()
emp2.name = "Somchai"
print("Employee1 Name: \(emp1.name)")  

//listing 8.10
class Employee {
    var name = "Empty"
    var age = 0
    func showinfo() -> String {
        return "Employee : \(name) \(age)"
    }
}
class PermanentEmployee : Employee {
    var departmentname = "Empty"    // พร็อพเพอตี้ที่เพิ่มเข้ามา
}
let emp1 =  PermanentEmployee()
emp1.name = "Weerachat"
emp1.age = 35
emp1.departmentname = "Finance"
var info = emp1.showinfo()
print("Info : \(info)")  // "Employee : Weerachart 35"


//listing 8.11
class Employee {
    var name = "Empty"
    var age = 0
    func showinfo() -> String {
        return "Employee : \(name) \(age)"
}
}
class PermanentEmployee : Employee {
    var departmentname = "Empty"
    override func showinfo() -> String {
        return "Employee : \(departmentname) \(name) \(age)"
}
}
let emp1 =  PermanentEmployee()
emp1.name = "Weerachat"
emp1.age = 35
emp1.departmentname = "Finance"
var info = emp1.showinfo()
print("Info : \(info)")


//listing 8.12
class Employee {
    var name = "Empty"
    var age = 0
    func showinfo() -> String {
        return "Employee : \(name) \(age)"
}
}
class PermanentEmployee : Employee {
    var departmentname = "Empty"
    override func showinfo() -> String {
        let olddata = super.showinfo()
        return "\(olddata) \(departmentname)"
}
}
let emp1 =  PermanentEmployee()
emp1.name = "Weerachat"
emp1.age = 35
emp1.departmentname = "Finance"
var info = emp1.showinfo()
print("Info : \(info)")  


//listing 8.13
class Employee {
    var name:String
    var age:Int
    init(name:String, age:Int){
        self.name = name
        self.age = age
    }
}
let emp1 = Employee(name: "Weerachart", age: 35)
print(emp1.name)
print(emp1.age)

//listing 8.14
class Employee {
    var name:String
    var age:Int
    init(name:String, age:Int){
        self.name = name
        self.age = age
    }
    convenience init() {
        self.init(name:"Empty", age:35)
    }
}
let emp1 = Employee()
print(emp1.name)
print(emp1.age)


//listing 8.15
class Product {
    var amount = 0
    var name = "Empty"
    var price = 0.0
    deinit {
        print("Instance was erased")
    }
}
var iphone6:Product? = Product ()
print(iphone6?.amount)
print(iphone6?.name)
print(iphone6?.price)
iphone6 = nil
print(iphone6?.amount)
print(iphone6?.name)
print(iphone6?.price)

//listing 8.16
enum Years:String {
    case one = "Freshy"
    case two = "Sophomore"
    case three = "Junior"
    case four = "Senior"
}
var studentYear = Years.three
print("You are a \(studentYear.rawValue)")  

//listing 8.17
enum Years:String {
    case one = "Freshy"
    case two = "Sophomore"
    case three = "Junior"
    case four = "Senior"
}
var studentYear = Years(rawValue:"Junior")
if studentYear == .three {
    print("Equals")
}

//listing 8.18
enum Years:Int {
    case one = 1
    case two = 2
    case three = 3
    case four = 4
    func showInfo() -> String {
        switch self {
            case .one:
                return "You are a Freshy"
            case .two:
                return "You are a Sophomore"
            case .three:
                return "You are a Junior"
            case .four:
                return "You are a Senior"
        }
    }
}
var studentYear = Years(rawValue:3)
if studentYear != nil {
    print(studentYear!.showInfo()) 
}

